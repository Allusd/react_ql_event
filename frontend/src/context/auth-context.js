import React from 'react';

// storage reactissa johon pääsee käsiksi kaikkialla

export default React.createContext({
    token: null,
    userId: null,
    login: (token, userId, tokenExpiration) => {},
    logout: () => {}
});