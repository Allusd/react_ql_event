const jwt = require('jsonwebtoken')

// juostaan jokaiselle requestille

// requesti , response ja next joka päästää requestin eteenpäin

module.exports = (req, res, next) => {

    // tarkastaa onko auth headeria
    const authHeader = req.get('Authorization');
    if(!authHeader){
        // annetaan false authiin
        // ja annetaan metadataa isAuth
        req.isAuth = false;
        return next();
    }
    // haetaan tokeni headerista
    // Bearer tokeni
    const token = authHeader.split(' ')[1];
    // jos ei tokenia tai empty stringi
    if(!token || token === ''){
        // annetaan false authiin
        // ja annetaan metadataa isAuth
        req.isAuth = false;
        return next();
    }
    let decodedToken;
    try{
        // jos tokeni niin vahvistetaan se
        decodedToken = jwt.verify(token, 'somesupersecretkey');
    }catch(err){
        req.isAuth = false;
        return next();
    }
    // jos vaikka joku virhe ja decodedToken ei setattu
    if(!decodedToken){
        req.isAuth = false;
        return next();
    }
    // jos tokeni annetaan true metadata
    // ja liitetään userId asioiden helpottamiseksi
    req.isAuth = true;
    req.userId = decodedToken.userId;
    next();

}