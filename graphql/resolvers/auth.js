const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken')

const User = require('../../models/user');

module.exports = {
  createUser: async args => {
    try {
      // tarkastetaan onko tietokannassa samalla sähköpostilla luotua käyttäjää jos on annetaan virhe
      const existingUser = await User.findOne({ email: args.userInput.email });
      if (existingUser) {
        throw new Error('User exists already.');
      }

      // luodaan hashatty salasana
      const hashedPassword = await bcrypt.hash(args.userInput.password, 12);

      // luodaan käyttäjä ja tallennetaan data
      const user = new User({
        email: args.userInput.email,
        password: hashedPassword
      });
      // tallennetaan käyttäjä tietokantaan
      const result = await user.save();

      return { ...result._doc, password: null, _id: result.id };
    } catch (err) {
      throw err;
    }
  },
  login: async ({email, password}) => {
    try{
      // haetaan käyttäjää kuten luomis tilanteessa jos ei löydy annetaan virhe
      const user = await User.findOne({email: email});
      if(!user){
        throw new Error('User does not exist');
      }
      // jos salasanat samat annetaan pääsy tietokantaan
      const isEqual = await bcrypt.compare(password, user.password)
      if(!isEqual){
        throw new Error('Password is invalid');
      }
      // luodaan tokeni käyttäjälle
      const createdToken = jwt.sign({userId: user.id, email: user.email}, 'somesupersecretkey', {
        expiresIn:'1h'
      });
      return { userId: user.id, token: createdToken, tokenExpiration: 1}
    }catch(err){
      throw err;
    }
  }
};